FROM fedora:38

RUN dnf install -y --nodocs --setopt install_weak_deps=False \
    latexmk \
    texlive-collection-latexrecommended \
    texlive-collection-fontsrecommended \
    texlive-collection-langczechslovak \
    texlive-adjustbox \
    texlive-algorithm2e \
    texlive-biblatex-iso690 \
    texlive-glossaries \
    texlive-mdframed \
    texlive-minted \
    texlive-multirow \
    texlive-pgfplots \
    texlive-stmaryrd \
    texlive-totalcount \
    && dnf clean all

ENV TEXMFVAR=/cache
RUN mkdir ${TEXMFVAR} && chmod a+rwx ${TEXMFVAR}

WORKDIR /data

VOLUME [ "/data", "/cache" ]
